package ru.leonova.tm.manager;

import ru.leonova.tm.entity.Project;

import java.util.List;

public class ProjectManager {

    public ProjectManager() {
    }

    public void createProject(String projectName, List<Project> projectList) {
        Project project = new Project(projectName);
        projectList.add(project);
    }

    public void showProjectList(List<Project> projectList) {
        int i = 0;
        for (Project project : projectList) {
            i++;
            System.out.println(i + ". " + project.getName());
        }
    }

     private Project findProjectByName(String name, List<Project> projectList) {
        for (Project project : projectList) {
            if (name.equalsIgnoreCase(project.getName())) {
                return project;
            }
        }
        return null;
    }

    public void deleteProjectByName(String name, List<Project> projectList) {
        Project p = findProjectByName(name, projectList);
        if (p != null) {
            System.out.println("Project " + p.getName() + " removed!");
            projectList.remove(p);
        }
    }

    public void deleteAllProjects(List<Project> projectList) {
        projectList.clear();
        System.out.println("All projects remove");
    }

    public void updateProject(String projectNameCur, String newProjectName, List<Project> projects) {
        for (Project p : projects) {
            if (p.getName().equalsIgnoreCase(projectNameCur)) {
                p.setName(newProjectName);
                System.out.println("Task " + projectNameCur + " modified like " + newProjectName);
            }
        }
    }
}

package ru.leonova.tm.manager;

import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;

import java.util.List;

public class TaskManager {
    private Project currentProject;

    public void createTask(String taskName, List<Project> projectList, List<Task> taskList) {
        currentProject = projectList.get(projectList.size() - 1);
        currentProject.addTask(taskName, taskList);
    }

    public void updateTaskName(String curTask, String taskName, List<Task> taskList) {
        for (Task t : taskList) {
            if (t.getName().equalsIgnoreCase(curTask)) {
                t.setName(taskName);
                System.out.println("Task " + curTask + " modified like " + taskName);
            }
        }
    }

    public void showTaskList(List<Task> taskList) {
        int i = 0;
        for (Task task : taskList) {
            i++;
            System.out.println(i + ". " + task.getName());
        }
    }

    public void deleteTaskByName(String name, List<Task> taskList) {
        for (Task t : taskList) {
            if (t.getName().equalsIgnoreCase(name)) {
                taskList.remove(t);
                System.out.println("Task " + name + " remove");
            }
        }
    }

    public void deleteAllTasks(List<Task> taskList) {
        taskList.clear();
        System.out.println("All tasks remove");
    }

}


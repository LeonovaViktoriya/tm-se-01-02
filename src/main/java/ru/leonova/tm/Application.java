package ru.leonova.tm;

import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.manager.ProjectManager;
import ru.leonova.tm.manager.TaskManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        ProjectManager projectManager = new ProjectManager();
        TaskManager taskManager = new TaskManager();
        List<Project> projectList = new ArrayList<Project>();
        List<Task> taskList = new ArrayList<Task>();
        Scanner scanner = new Scanner(System.in);
        String text;
        do {
            text = scanner.nextLine();
            if ("create-p".equalsIgnoreCase(text.toLowerCase())) {
                System.out.print("[CREATE PROJECT]\nINTER NAME:\n");
                text = scanner.nextLine();
                projectManager.createProject(text, projectList);
                System.out.println("[OK]");
            } else if ("create-t".equalsIgnoreCase(text.toLowerCase())) {
                if(!projectList.isEmpty()){
                    System.out.println("[CREATE TASK]\nINTER NAME:");
                    text = scanner.nextLine();
                    taskManager.createTask(text, projectList, taskList);
                    System.out.println("[OK]");
                }else {
                    System.out.println("The task cannot exist outside the project");
                }
            }else if ("list-p".equalsIgnoreCase(text.toLowerCase())) {
                if(!projectList.isEmpty()) {
                    System.out.println("\n[LIST PROJECT]");
                    projectManager.showProjectList(projectList);
                }else {
                    System.out.println("\nYou have no projects");
                }
            }else if ("list-t".equalsIgnoreCase(text.toLowerCase())) {
                if(!projectList.isEmpty()) {
                    System.out.println("\n[LIST TASKS]");
                    taskManager.showTaskList(taskList);
                }else {
                    System.out.println("\nYou have no projects");
                }
            }else if ("del-p".equalsIgnoreCase(text.toLowerCase())) {
                if(!projectList.isEmpty()) {
                    projectManager.showProjectList(projectList);
                    System.out.println("[DELETE PROJECT BY NAME]\nINNER NAME:");
                    String name = scanner.nextLine();
                    projectManager.deleteProjectByName(name, projectList);
                }else{
                    System.out.println("\nYou have no projects");
                }
            }else if ("del-t".equalsIgnoreCase(text.toLowerCase())) {
                if(!taskList.isEmpty()) {
                    taskManager.showTaskList(taskList);
                    System.out.println("[DELETE TASK BY NAME]\nINNER NAME:");
                    String nameCurTask = scanner.nextLine();
                    taskManager.deleteTaskByName(nameCurTask, taskList);
                }
                else{
                    System.out.println("\nYou have no tasks");
                }
            }
            else if ("up-p".equalsIgnoreCase(text.toLowerCase())) {
                if(!projectList.isEmpty()) {
                    System.out.println("\n[UPDATE NAME PROJECT]\nINTER NAME CURRENT PROJECT:");
                    String nameCur = scanner.nextLine();
                    System.out.println("\nINTER NEW NAME FOR THIS PROJECT:");
                    String nameNew = scanner.nextLine();
                    projectManager.updateProject(nameCur,nameNew,projectList);
                }else {
                    System.out.println("\nYou have no projects");
                }
            }else if ("up-t".equalsIgnoreCase(text.toLowerCase())) {
                if(!taskList.isEmpty()) {
                    System.out.println("\n[UPDATE NAME TASK]\nINTER NAME CURRENT TASK:");
                    String nameCurTask = scanner.nextLine();
                    System.out.println("INTER NEW NAME CURRENT TASK:");
                    String nameNew = scanner.nextLine();
                    taskManager.updateTaskName(nameCurTask, nameNew, taskList);
                }else {
                    System.out.println("\nYou have no tasks");
                }
            }
            else if ("del-all-p".equalsIgnoreCase(text.toLowerCase())) {
                projectManager.deleteAllProjects( projectList);
            }else if ("del-all-t".equalsIgnoreCase(text.toLowerCase())) {
                taskManager.deleteAllTasks(taskList);
            }else if ("help".equalsIgnoreCase(text.toLowerCase())) {
                System.out.println("\nhelp - show available commands");
                System.out.println("\ncreate-p - create new project");
                System.out.println("\ncreate-t - create new task for current project");
                System.out.println("\nlist-p - show project list");
                System.out.println("\nlist-t - show project list");
                System.out.println("\ndel-p - delete project by id");
                System.out.println("\nup-p - update project");
                System.out.println("\nup-t - update project's task's name");
                System.out.println("\ndel-t - delete current project's task");
                System.out.println("\ndel-all-t - delete all task");
                System.out.println("\ndel-all-p - delete all projects");
            }
        }
        while (!text.equals("stop"));
    }

}

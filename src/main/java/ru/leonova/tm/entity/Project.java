package ru.leonova.tm.entity;

import java.util.List;

public class Project {

    private String name;

    public Project(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addTask(String taskName, List<Task> taskList ) {
        Task newTask = new Task(taskName);
        taskList.add(newTask);
    }

}

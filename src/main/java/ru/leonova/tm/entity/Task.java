package ru.leonova.tm.entity;

public class Task {

    private String name;

    Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}

